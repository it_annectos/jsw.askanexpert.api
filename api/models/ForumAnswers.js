/**
 * ForumAnswers.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'ForumAnswers',
    attributes: {
        answerId: {
            type: 'string',
            required: true
        },
        questionId: {
            type: 'string',
            required: true
        },
        answer: {
            type: 'string',
            required: true
        },
        userId: { // engineer Id who is answering
            type: 'string'
        },
        clientId: {
            type: 'string'
        },
        programId: {
            type: 'string'
        },
        createdBy: {
            type: 'string'
        },
        updatedBy: {
            type: 'string'
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        }
    },

    addAnswer: function(reqBody, next) {
        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        answerInfo = {};
        answerInfo.answerId = "ANS" + Date.now();
        answerInfo.questionId = reqBody.questionId;
        answerInfo.answer = reqBody.answer;
        answerInfo.userId = reqBody.userId;
        answerInfo.clientId = reqBody.clientId;
        answerInfo.programId = reqBody.programId;
        answerInfo.createdBy = reqBody.createdBy;
        answerInfo.updatedBy = reqBody.updatedBy;

        ForumAnswers.create(answerInfo).exec(function(err, record) {
            if (err) { // handling for error
                responseObj.message = err;
                sails.log.error("ForumAnswers>addAnswer>ForumAnswers.create ", "Input:" + answerInfo, " error:" + err);
                next(responseObj);
            } else if (!record) { // handling in case undefined/no object was returned
                responseObj.message = "undefined object was returned";
                sails.log.error("ForumAnswers-Model>addAnswer>ForumAnswers.create ", "Input:" + answerInfo, " result:" + record);
                next(responseObj);
            } else { // record created successfully	

                // var reqObj = {
                //     "updateInfo": {
                //         "answeredBy": reqBody.userId,
                //         "isAnswered": true,
                //     },
                //     "questionId": reqBody.questionId
                // };


                // reqObj.clientId = reqBody.clientId;
                // reqObj.programId = reqBody.programId;


                // ForumQuestions.updateForumQuestion(reqObj, function(response) {
                //     console.log("response", response);
                //     if (response.statusCode === 0) {
                //         responseObj.statusCode = 0;
                //         responseObj.message = "Record successfully created";
                //         responseObj.result = record;
                //         sails.log.info("ForumAnswers-Model>addAnswer>ForumAnswers.addAnswer : record created successfully!!");
                //     } else {
                //         responseObj = response;
                //     }
                //     next(responseObj);

                // });
                responseObj.statusCode = 0;
                responseObj.message = "Record successfully created";
                responseObj.result = record;
                next(responseObj);


            }
        }); // end of ForumAnswers.create
    },

    updateAnswer: function(reqBody, next) {
        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };
        var findCriteria = { answerId: reqBody.answerId, isDeleted: false };
        var updateInfo = reqBody.updateInfo;

        ForumAnswers.update(findCriteria, updateInfo).exec(function(err, records) {
            console.log("updateAnswer: updated record is", records);
            if (err) {
                responseObj.message = err;
                sails.log.error("ForumAnswers-Model>updateAnswer>ForumAnswers.update ", "Input:" + updateInfo, " error:" + err);
                next(responseObj);
            } else if (!records) { // handling in case undefined/no object was returned
                responseObj.message = "undefined object was returned";
                sails.log.error("ForumAnswers-Model>updateAnswer>ForumAnswers.update ", "Input:" + updateInfo, " result:" + records);
                next(responseObj);
            } else if (records.length === 0) { // handling in case records was not updated
                responseObj.message = "record not found!!";
                sails.log.error("ForumAnswers-Model>updateAnswer>ForumAnswers.update ", "Input:" + updateInfo, " result:" + records);
                next(responseObj);
            } else { // record successfully updated
                responseObj.statusCode = 0;
                responseObj.message = "record updated successfully!!";
                responseObj.result = records[0];
                sails.log.info("ForumAnswers-Model>updateAnswer>ForumAnswers.update : record updated successfully!!");
                next(responseObj);
            }
        });
    },

    getAnswer: function(reqBody, next) {
        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };
        //question array 
        var QuestioAnswers = [];
        var queryString = [{
            $match: {
                isDeleted: false
            }
        }];
        if (reqBody.skip !== undefined) {
            queryString.push({ $skip: reqBody.skip });
        }
        if (reqBody.limit !== undefined) {
            queryString.push({ $limit: reqBody.limit });
        }
        // user Id filter
        if (reqBody.userId) {
            queryString[0].$match.userId = reqBody.userId;
        }
        // question Id filter
        if (reqBody.questionId) {
            queryString[0].$match.questionId = reqBody.questionId;
        }

        // to get multiple questions
        if (reqBody.questionIdArray) {
            queryString[0].$match.questionId = { "$in": reqBody.questionIdArray };
        }

        // to get the question for which the answer belongs
        queryString.push({
            $lookup: {
                from: "ForumQuestions",
                localField: "questionId",
                foreignField: "questionId",
                as: "questionInfo"
            }
        });
        queryString.push({
            $unwind: {
                path: "$questionInfo",
                preserveNullAndEmptyArrays: true
            }
        })

        // to get the question for which the answer belongs
        queryString.push({
            $lookup: {
                from: "ProgramUsers",
                localField: "userId",
                foreignField: "programUserId",
                as: "userInfo"
            }
        });
        queryString.push({
            $unwind: {
                path: "$userInfo",
                preserveNullAndEmptyArrays: true
            }
        })
        queryString.push({
            $project: {
                "answerId": "$answerId",
                "questionId": "$questionId",
                "answer": "$answer",
                "userId": "$userId",
                "clientId": "$clientId",
                "programId": "$programId",
                "createdBy": "$createdBy",
                "updatedBy": "$updatedBy",
                "createdAt": "$createdAt",
                "updatedAt": "$updatedAt",
                "questionInfo": "$questionInfo",
                "programUserId": "$userInfo.programUserId",
                "userId": "$userInfo.userDetails.userId",
                "userName": "$userInfo.userDetails.userName",
                "MobileNumber": "$userInfo.userDetails.mobileNumber",
                "emailId": "$userInfo.userDetails.email",
                "state": "$userInfo.userDetails.state",
                "city": "$userInfo.userDetails.city"
            }
        });

        ForumAnswers.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    responseObj.queryString = queryString;
                    console.log("records,err", records, err);
                    if (err) {
                        responseObj.message = err;
                        sails.log.error("ForumAnswers-Model>updateAnswer>ForumAnswers.update ", "Input:" + queryString, " result:" + err);
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        sails.log.error("ForumAnswers-Model>updateAnswer>ForumAnswers.update ", "Input:" + queryString, " result:" + records);
                        next(responseObj);
                    } else if (records.length === 0) {
                        responseObj.statusCode = 2;
                        responseObj.message = "No records found !!";
                        sails.log.error("ForumAnswers-Model>updateAnswer>ForumAnswers.update ", "Input:" + queryString, " result:" + records);
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "Answers were fetched successfully";
                        sails.log.info("ForumAnswers-Model>getAnswer>ForumAnswers.aggregate : record fetched successfully!!");
                        responseObj.result = records;
                        next(responseObj);
                    }
                })
            }
        })
    },

    displaylastForumQuesforengineers: function(expertengineers, skip, limit, next) {
        console.log("Engineer ", expertengineers);

        var responseObject = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var queryString = [{
            $match: {
                "userId": expertengineers
            }
        }];

        queryString.push({
            $sort: {
                "updatedAt": -1
            }
        });

        if (skip !== undefined) {
            queryString.push({ $skip: skip });
        }

        if (limit !== undefined) {
            queryString.push({ $limit: 5 });
        }

        queryString.push({
            $lookup: {
                "from": "ForumQuestions",
                "localField": "questionId",
                "foreignField": "questionId",
                "as": "user1"
            }
        });
        queryString.push({
            $lookup: {
                from: "ProgramUsers",
                localField: "userId",
                foreignField: "programUserId",
                as: "userInfo"
            }
        });
        queryString.push({
            $unwind: {
                path: "$userInfo",
                preserveNullAndEmptyArrays: true
            }
        });

        queryString.push({
            $project: {
                "_id": 0,
                "questionId": "$user1.questionId",
                "question": "$user1.question",
                "answerId": "$answerId",
                "answer": "$answer",
                "question_askedby": "$user1.frontendUserInfo.name",
                "question_emailIdby": "$user1.frontendUserInfo.email",
                "question_mobileNumberby": "$user1.frontendUserInfo.mobileNumber",
                "question_zipcodeby": "$user1.frontendUserInfo.zipcode",
                "isAnswered": "$user1.isAnswered",
                "answeredAt": "$user1.updatedAt",
                "programUserId": "$userInfo.programUserId",
                "userId": "$userId",
                "answeredBy": "$userInfo.userDetails.userName",
                "MobileNumber": "$userInfo.userDetails.mobileNumber",
                "emailId": "$userInfo.userDetails.email",
                "state": "$userInfo.userDetails.state",
                "city": "$userInfo.userDetails.city"
            }
        });

        queryString.push({
            $unwind: {
                path: "$question_askedby",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$question_emailIdby",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$question_mobileNumberby",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$question_zipcodeby",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$questionId",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$question",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$isAnswered",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$answeredAt",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$answeredBy",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$state",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $unwind: {
                path: "$city",
                preserveNullAndEmptyArrays: true // optional
            }
        });


        /*We need to fetch last 5 questions and make a lookup and get the 
         * last answer for this question and merge the question and answer 
         * in array and send the result */
        ForumAnswers.native(function(err, collection) {
            if (err) {
                responseObject.statusCode = -1;
                responseObject.message = "Server Error";
                next(responseObject)
            } else {
                collection.aggregate(queryString, function(err, lastquestionIdResult) {
                    if (err) {
                        console.log(err);
                        responseObject.statusCode = -1;
                        responseObject.message = "Server Error";
                        next(responseObject);
                    } else {
                        console.log("Last 5 QuestionId received", lastquestionIdResult);
                        responseObject.statusCode = 0;
                        responseObject.message = "Last 5 Question and Answer fetched by expertengineers";
                        responseObject.result = lastquestionIdResult;
                        next(responseObject);
                    } //endof function callback
                });
            }
        });
    },

};