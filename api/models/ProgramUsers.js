/**
 * ProgramUsers.js
 *
 * @description :: 
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var request = require('request');
module.exports = {
    tableName: 'ProgramUsers',
    attributes: {},

    createExpertProfileDetails: function(request, response) {
        var responseObject = {};
        var profileExpertDetailsObject = {};

        console.log("Program Id ", request.programId);
        console.log("Profile Id ", request.profileId);
        if ((request.programId < 0) && (request.profileId < 0)) {
            responseObject.statusCode = -1;
            responseObject.message = "Server Error";
            response(responseObject);
        } else {
            console.log(request);
            var matchCriteria = {
                "programId": request.programId,
                "profileId": request.profileId,
                "programUserId": request.programUserId
            }
            console.log("matchCriteria 1 ", matchCriteria);
            profileExpertDetailsObject.Images = request.profileImage;
            profileExpertDetailsObject.expertise = request.profileExpertise;
            profileExpertDetailsObject.Details = request.profileDetails;
            console.log("update ", profileExpertDetailsObject);
            ProgramUsers.update({ "programId": request.programId, "profileId": request.profileId, "programUserId": request.programUserId }, { "profileDetails": profileExpertDetailsObject }, { multi: true }).exec(function(err, profileExpertDetails) {
                if (err) {
                    responseObject.statusCode = -1;
                    responseObject.message = "Server Error";
                    response(responseObject);
                } else {
                    responseObject.statusCode = 2;
                    responseObject.message = "Update data successful";
                    responseObject.result = profileExpertDetailsObject;
                    response(responseObject);
                }
                //}
            });
        } //endof else
    },

    updateExpertProfileDetails: function(profileId, programId, response) {
        var responseObject = {};

        if (programId < 0) {
            responseObject.statusCode = -1;
            responseObject.message = "Server Error";
            response(responseObject);
        } else {
            console.log("PgmID ", programId);
            console.log("ProfileID ", profileId);

            ProgramUsers.native(function(err, collection) {
                if (err) {
                    responseObject.statusCode = -1;
                    responseObject.message = "Server Error";
                    response(responseObject)
                } else {
                    collection.aggregate( // Stage 1
                        {
                            $match: {
                                "programId": "PR1518085378956",
                                "profileId": profileId
                            }
                        },
                        // Stage 2
                        {
                            $project: {
                                "programId": "$programId",
                                "profileId": "$profileId",
                                "programUserId": "$programUserId",
                                "userId": "$userDetails.userId",
                                "userName": "$userDetails.userName",
                                "MobileNumber": "$userDetails.mobileNumber",
                                "emailId": "$userDetails.email",
                                "profileImage": { $ifNull: ["$profileDetails.Images", "https://s3-ap-southeast-1.amazonaws.com/cdn-annectos/askanexpert/defaultuser.jpg"] },
                                "profileExpertise": "$profileDetails.expertise",
                                "profileDetails": "$profileDetails.Details"
                            }
                        },
                        function(err, profileExpertDetails) {
                            var profileDetails = [];
                            var userDetails = [];
                            if (profileExpertDetails.length > 0) {
                                for (var a = 0; a < profileExpertDetails.length; a++) {
                                    var profileExpertDetailsObject = {};
                                    var ExpertprofileId = profileExpertDetails[a].profileId;

                                    profileExpertDetailsObject.Image = profileExpertDetails[a].profileImage;
                                    perprofileExpertDetailsObject.profileExpertise = profileExpertDetails[a].profileExpertise;
                                    perprofileExpertDetailsObject.profileDetails = profileExpertDetails[a].profileDetails;
                                    //perprofileExpertDetails = profileExpertDetails[a].;

                                    var matchCriteria = {
                                        "programId": profileExpertDetails[a].programId,
                                        "profileId": profileExpertDetails[a].profileId,
                                        "isApproved": true
                                    };

                                    collection.update(matchCriteria, {
                                            $set: {
                                                "profileDetails": profileExpertDetailsObject
                                            },
                                        }, { upsert: false, multi: true },
                                        function(err, Resultfinal) {
                                            if (err) {
                                                console.log("err", err);
                                                responseObject.statusCode = 3;
                                                responseObject.message = "Update data unsuccessful";
                                                response(responseObject);
                                            } else {
                                                console.log("Data updated", Resultfinal);
                                            }
                                        });

                                } //Endof For Loop
                                responseObject.statusCode = 2;
                                responseObject.message = "Update data successful";
                                responseObject.result = Resultfinal;
                                response(responseObject);
                            }
                        });
                }
            }); //endof native
        } //endof else
    },

    showexpertProfileDetails: function(request, response) {
        var responseObject = {};
        var programId = request.programId;
        var engineerId = request.engineerId;

        if (programId < 0) {
            responseObject.statusCode = -1;
            responseObject.message = "Server Error";
            response(responseObject);
        } else {
            console.log("PgmID ", programId);

            ProgramUsers.native(function(err, collection) {
                if (err) {
                    responseObject.statusCode = -1;
                    responseObject.message = "Server Error";
                    response(responseObject)
                } else {
                    collection.aggregate( // Stage 1
                        {
                            $match: {
                                "programUserId": engineerId
                            }
                        },
                        // Stage 2
                        {
                            $project: {
                                "programId": "$programId",
                                "profileId": "$profileId",
                                "programUserId": "$programUserId",
                                "userId": "$programUserId",
                                "FirstName": "$businessEntityUserDetails.firstName",
                                "LastName": "$businessEntityUserDetails.lastName",
                                "MobileNumber": "$businessEntityUserDetails.mobileNumber",
                                "emailId": "$businessEntityUserDetails.email",
                                "profileImage": { $ifNull: ["$profileDetails.Images", "https://s3-ap-southeast-1.amazonaws.com/cdn-annectos/askanexpert/defaultuser.jpg"] },
                                "profileExpertise": "$profileDetails.expertise",
                                "profileDetails": "$profileDetails.Details"
                            }
                        },
                        function(err, profileExpertDetails) {
                            if (err) {
                                console.log("err", err);
                                responseObject.statusCode = 3;
                                responseObject.message = "Read Profile data unsuccessful";
                                response(responseObject);
                            } else {
                                console.log("Data updated", profileExpertDetails);
                                responseObject.statusCode = 2;
                                responseObject.message = "Read Profile data successful";
                                responseObject.result = profileExpertDetails;
                                response(responseObject);
                            }
                        }
                    );
                }
            }); //endof native
        } //endof else
    },

    getExpertList: function(request, response) {
        var responseObject = {};
        /*
        Assuming the Program ID and Profile ID are part of Webapp configuration
        */
        var programId = request.body.programId;
        var profileId = request.body.profileId;

        if (programId.length < 0) {
            responseObject.statusCode = -1;
            responseObject.message = "Server Error";
            response(responseObject);
        } else {
            console.log("PgmID ", programId);
            console.log("profileId ", profileId);

            ProgramUsers.native(function(err, collection) {
                if (err) {
                    responseObject.statusCode = -1;
                    responseObject.message = "Server Error";
                    response(responseObject)
                } else {
                    collection.aggregate( // Stage 1
                        {
                            $match: {
                                "programId": programId,
                                "profileId": profileId
                                    //"programId": "PR1518085378956"
                            }
                        },
                        // Stage 2
                        {
                            $project: {
                                "programId": "$programId",
                                "profileId": "$profileId",
                                "programUserId": "$programUserId",
                                "userId": "$programUserId",
                                "FirstName": "$businessEntityUserDetails.firstName",
                                "LastName": "$businessEntityUserDetails.lastName",
                                "MobileNumber": "$businessEntityUserDetails.mobileNumber",
                                "emailId": "$businessEntityUserDetails.email",
                                "profileImage": { $ifNull: ["$profileDetails.Images", "https://s3-ap-southeast-1.amazonaws.com/cdn-annectos/askanexpert/defaultuser.jpg"] },
                                "profileExpertise": "$profileDetails.expertise",
                                "profileDetails": "$profileDetails.Details"
                            }
                        },
                        function(err, profileExpertDetails) {
                            if (err) {
                                console.log("err", err);
                                responseObject.statusCode = 3;
                                responseObject.message = "Read Users data unsuccessful";
                                response(responseObject);
                            } else {
                                console.log("Data updated", profileExpertDetails);
                                responseObject.statusCode = 2;
                                responseObject.message = "Read data successful";
                                responseObject.result = profileExpertDetails;
                                response(responseObject);
                            }
                        }
                    );
                }
            }); //endof native
        } //endof else
    },
}